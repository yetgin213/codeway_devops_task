require('dotenv').config();
const express = require('express');
const { Pool } = require('pg');

const app = express();
const pool = new Pool({
  connectionString: `postgresql://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`,
});

app.get('/', async (req, res) => {
  try {
    const client = await pool.connect();
    await client.query('SELECT NOW()'); // Simple query to test the connection
    client.release();
    res.send('DB connection successful');
  } catch (err) {
    res.status(500).send('DB connection failed: ' + err.message);
  }
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
